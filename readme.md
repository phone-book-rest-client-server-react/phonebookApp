###PhoneBook Application

Simple text JSON file used in REST emulation for a clientside phonebook application with search functionality.

## Prerequisites

A current version of node is required to run this application. There are two applications that are submodules to this repo that are run simultaneously.

### Clone the application

```
git@gitlab.com:phone-book-rest-client-server-react/phonebookApp.git
cd phonebookApp
yarn run firstRun
yarn start
```

### Yarn Start

When you run yarn or npm start from this repo, the prestart script runs yarn install and then updates the submodules, which then run yarn install and yarn start within their project folders. If you do not have yarn installed, run npm install, and it will handle the required yarn installation for the applications.

The rest-server repo provides a simple JSON server using json-server.

The rest-client repo provides a React App which accesses the json-server to populate the PhoneBook.

### Testing the app

When you run yarn test from this directory, it calls scripts which initiate the scripts in the submodule directory. At this time, only the rest-client repo has tests.

```
yarn run test
```

### Run the Application

The application should be testable in localhost:3000 after the yarn start scripts have completed. localhost:3001/phonebook provides access to the phone numbers.

rest-client uses port 3000.
rest-server uses port 3001.

## Contact

kevin.ready@gmail.com
